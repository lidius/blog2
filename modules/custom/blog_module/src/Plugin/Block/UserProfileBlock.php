<?php

namespace Drupal\blog_module\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\UncacheableDependencyTrait;
use Exception;

/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "user_profile_block",
 *   admin_label = @Translation("User Profile Block"),
 * )
 */
class UserProfileBlock extends BlockBase {

    // Nice stuff!
    use UncacheableDependencyTrait;

    /**
     * Build Function.
     */
    public function build() {

        $current_user = \Drupal::currentUser(); 
        $user_mail = $current_user->getEmail();

        try {
            return [
                '#markup' => $this->t($user_mail),
            ];

        } catch (Exception $e) {

            return [
                '#markup' => $this->t('Could not identify an email address!'),
            ];
        }

    }

}